

function HeaderComponent() {
   
        return (
            <>
                <div>
                    <div className="container" id="home">
                        <div className="row">
                            <div className="col-sm-12">
                                <nav className="navbar navbar-expand-lg navbar-light fixed-top ">
                                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                                        <ul className="navbar-nav nav-fill w-100">
                                            <a className="nav-item nav-link" href="#home">Trang chủ </a>
                                            <a className="nav-item nav-link" href="#combo">Combo</a>
                                            <a className="nav-item nav-link" href="#loaipizza">Loại pizza</a>
                                            <a className="nav-item nav-link" href="#guidonhang">Gửi đơn hàng</a>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        )
    
}

export default HeaderComponent;
