
function ContentFooter(){
   
        return (
            <>
                <div className="container-fluid p-5" style={{ marginTop: "100px", backgroundColor: "orange" }}>
                    <div className="row text-center">
                        <div className="col-sm-12">
                            <h4 className="m-2">Footer</h4>
                            <a href="#" className="btn btn-dark m-3"><i className="fa fa-arrow-up"></i>To the top</a>
                            <div className="m-2">
                            
                            </div>
                            <h6 className="m-2">Powered by DEVCAMP</h6>
                        </div>
                    </div>
                </div>

            </>
        )
    
}

export default ContentFooter;
