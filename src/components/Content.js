
import IntroduceComponent from "./ContentComponent/IntroduceComponent.js";
import TypeComponent from "./ContentComponent/TypeComponent.js";
import SizeComponent from "./ContentComponent/SizeComponent.js";
import DrinkComponent from "./ContentComponent/DrinkComponent.js";
import FormComponent from "./ContentComponent/FormComponent.js";
import ButtonSubmit from "./ContentComponent/ButtonSubmit.js";

import { useEffect, useState } from "react";

import { Button } from "reactstrap";


function ContentComponent() {
    const [sizeCombo, setSizeCombo] = useState("")
    const [typePizza, setTypePizza] = useState("")
    const [drink, setDrink] = useState("")
    const [info, setInfo] = useState("")

    const callbackFunctionSize = (childData) => {
        setSizeCombo(childData)
        console.log(childData)
    }
    const callbackFunctionType = (childData) => {
        setTypePizza(childData)
        console.log(childData)
    }
    const callbackFunctionDrink = (childData) => {
        setDrink(childData)
        console.log(childData)
    }
    const callbackFunctionInfo = (childData) => {
        setInfo(childData)
        console.log(childData)
    }

    return (
        <>
            <IntroduceComponent />
            <SizeComponent parentCallback={callbackFunctionSize} />
            <TypeComponent parentCallback={callbackFunctionType} />
            <DrinkComponent parentCallback={callbackFunctionDrink} />
            <FormComponent parentCallback={callbackFunctionInfo} />
            <ButtonSubmit sizeCombo={sizeCombo} typePizza={typePizza} drink={drink} info={info} />
        </>
    )

}

export default ContentComponent;
