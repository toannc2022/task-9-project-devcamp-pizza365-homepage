import pic1 from "../../images/1.jpg";
import pic2 from "../../images/2.jpg";
import pic3 from "../../images/3.jpg";
import pic4 from "../../images/4.jpg";
import {
    UncontrolledCarousel
} from 'reactstrap';

function Lading() {
    return (
        <>

            <UncontrolledCarousel
                items={[
                    {
                        altText: '',
                        caption: '',
                        key: 1,
                        src: pic1
                    },
                    {
                        altText: '',
                        caption: '',
                        key: 2,
                        src: pic2
                    },
                    {
                        altText: '',
                        caption: '',
                        key: 3,
                        src: pic3
                    },
                    {
                        altText: '',
                        caption: '',
                        key: 4,
                        src: pic4
                    },
                ]}
            />
        </>
    )
}

export default Lading;