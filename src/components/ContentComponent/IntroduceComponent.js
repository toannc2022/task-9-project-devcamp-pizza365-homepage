

  import Lading from "./Lading.js"

function IntroduceComponent(){
   
        return (
            <>
                <div className="container">
                    <div className="logopizza365">
                        <h2><b>PIZZA 365</b></h2>
                        <p className="innghien">Truly italian !</p>
                    </div>
                </div>
                <div className="container">
                   <Lading/>
                        
                </div>
                <div className="container">
                    <div className="col-sm-12 text-center p-4 mt-4">
                        <h2><span className="tieude365 p-2">Tại sao lại Pizza 365</span></h2>
                    </div>
                </div>

                <div className="container">
                    <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-3 p-4 border border-warning" style={{ backgroundColor: "lightgoldenrodyellow" }}>
                                <h3 className="p-2">Đa dạng</h3>
                                <p className="p-2">Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.</p>
                            </div>
                            <div className="col-sm-3 p-4 border border-warning" style={{ backgroundColor: "yellow" }}>
                                <h3 className="p-2">Chất lượng</h3>
                                <p className="p-2">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
                            </div>
                            <div className="col-sm-3 p-4 border border-warning" style={{ backgroundColor: "lightsalmon" }}>
                                <h3 className="p-2">Hương vị</h3>
                                <p className="p-2">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.</p>
                            </div>
                            <div className="col-sm-3 p-4 border border-warning" style={{ backgroundColor: "orange" }}>
                                <h3 className=" p-2">Dịch vụ</h3>
                                <p className="p-2">Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân tiến.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </>
        )
    
}

export default IntroduceComponent;
