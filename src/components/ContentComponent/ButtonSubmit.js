import * as React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { useEffect, useState } from "react";
import { textAlign } from '@mui/system';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function ButtonSubmit(props) {
    var voucherID = props.info.magiamgia;
    const [phanTramGiamGia, setPhanTramGiamGia] = useState("");
    const [maDonHang, setMaDonHang] = useState("");


    const getDataVoucher = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + voucherID);
        const data = await response.json();
        return data;
    }

    const guidonClick = (event) => {
        console.log(props)

        if (validate(props)) {
            getDataVoucher()
                .then((data) => {
                    console.log(data);
                    setPhanTramGiamGia(data.phanTramGiamGia);
                })
                .catch((error) => {
                    console.log(error);
                    setPhanTramGiamGia("0");
                })

            setOpen(true);
        }

    }

    const validate = (event) => {
        if (event.sizeCombo == "") {
            console.log("chưa nhập sizeCombo")
            alert("Vui lòng chọn SizeCombo ")
            return false;
        }
        if (event.typePizza == "") {
            console.log("chưa nhập typePizza")
            alert("Vui lòng chọn TypePizza ")
            return false;
        }
        if (event.drink === "") {
            console.log("chưa nhập drink")
            alert("Vui lòng chọn Nước Uống ")
            return false;
        }
        if (event.info.ten === "") {
            console.log("chưa nhập họ tên")
            alert("Vui lòng nhập họ tên ")
            return false;
        }
        if (event.info.email === "") {
            console.log("chưa nhập email")
            alert("Vui lòng nhập email ")
            return false;
        }
        if (event.info.sodienthoai === "") {
            console.log("chưa nhập sodienthoai")
            alert("Vui lòng nhập Số Điện Thoại ")
            return false;
        }
        if (event.info.diachi === "") {
            console.log("chưa nhập diachi")
            alert("Vui lòng nhập Địa Chỉ ")
            return false;
        }
        if (event.info.magiamgia === "") {
            console.log("chưa nhập magiamgia")
        }
        if (event.info.loinhan === "") {
            console.log("chưa nhập loinhan")
        }
        return true;
    }

    const infoOrder = props;

    const [open, setOpen] = React.useState(false);
    const [openModal, setOpenModal] = React.useState(false);
    const handleClose = () => setOpen(false);
    const handleCloseModal = () => {
        setOpenModal(false);
        window.location.reload();
    }


    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }

    const xacNhanClick = () => {

        const body = {
            method: 'POST',
            body: JSON.stringify({
                kichCo: props.sizeCombo.kichCo,
                duongKinh: props.sizeCombo.duongKinh,
                suon: props.sizeCombo.suon,
                salad: props.sizeCombo.salad,
                loaiPizza: props.typePizza.loaiPizza,
                idVourcher: props.info.magiamgia,
                idLoaiNuocUong: props.drink,
                soLuongNuoc: props.sizeCombo.soLuongNuoc,
                hoTen: props.info.ten,
                thanhTien: props.sizeCombo.thanhTien,
                email: props.info.email,
                soDienThoai: props.info.sodienthoai,
                diaChi: props.info.diachi,
                loiNhan: props.info.loinhan
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders', body)
            .then((data) => {
                console.log(data);
                setMaDonHang(data.orderId)
                setOpenModal(true);
            })
            .catch((error) => {
                console.log(error);
            })

        alert("Đã tạo thành công đơn hàng!");
        setOpen(false);
    }


    return (
        <>
            <div className="container">
                <button type="button" className="btn bg-warning w-100" id="btn-guidonhang" onClick={guidonClick}>Gửi</button>
            </div>

            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Thông tin đơn hàng:
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        <hr />
                        {"SizeCombo : " + props.sizeCombo.kichCo} <br />
                        {"TypePizza : " + props.typePizza.loaiPizza} <br />
                        {"Nước Uống : " + props.drink} <br />
                        {"Tên : " + props.info.ten} <br />
                        {"Email : " + props.info.email} <br />
                        {"Số Điện Thoại : " + props.info.sodienthoai} <br />
                        {"Địa Chỉ : " + props.info.diachi}<br />
                        {"Mã Giảm Giá : " + props.info.magiamgia}<br />
                        {" Phần Trăm Giảm Giá : " + phanTramGiamGia + "%"}  <br />
                        {"Lời nhắn : " + props.info.loinhan} <br />
                        <hr />
                    </Typography>
                    <Typography className='text-center'>
                        <button className='btn btn-success' onClick={xacNhanClick}>Xác nhận</button>
                    </Typography>


                </Box>
            </Modal>

            <Modal
                open={openModal}
                onClose={handleCloseModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Order đơn hàng thành công ! <hr />
                        Mã đơn hàng của bạn là : {maDonHang}  <hr />

                    </Typography>
                    <Typography textAlign={"right"}>
                        <button className='btn btn-success' onClick={handleCloseModal}> OK </button>
                    </Typography>

                </Box>
            </Modal>
        </>
    )

}

export default ButtonSubmit;

