import { useEffect, useState } from "react";
import ButtonSubmit from "./ButtonSubmit.js";


function FormComponent(props) {
    const [ten, setTen] = useState("");
    const [email, setEmail] = useState("");
    const [sodienthoai, setSodienthoai] = useState("");
    const [diachi, setDiachi] = useState("");
    const [magiamgia, setMagiamgia] = useState("");
    const [loinhan, setLoinhan] = useState("");

    const tenChangeHandler = (event) => {
        setTen(event.target.value);
        console.log("Tên : " + event.target.value)
    }
    const emailChangeHandler = (event) => {
        setEmail(event.target.value);
        console.log("Email : " + event.target.value)

    }
    const sodienthoaiChangeHandler = (event) => {
        setSodienthoai(event.target.value);
        console.log("Số điện thoại : " + event.target.value)

    }
    const diachiChangeHandler = (event) => {
        setDiachi(event.target.value);
        console.log("Địa chỉ : " + event.target.value)

    }
    const voucherChangeHandler = (event) => {
        setMagiamgia(event.target.value);
        console.log("Mã giảm giá (voucher ID) : " + event.target.value)

    }
    const mesageChangeHandler = (event) => {
        setLoinhan(event.target.value);
        console.log("Lời nhắn : " + event.target.value)
    }
    useEffect(() => {
        props.parentCallback({ ten, email, sodienthoai, diachi, magiamgia, loinhan });

    }, [ten, email, sodienthoai, diachi, magiamgia, loinhan ])


    return (
        <>
            <div className="container" id="guidonhang">
                <div className="col-sm-12 text-center p-4 mt-4">
                    <h2><span className="tieude365 p-2">Gửi đơn hàng</span></h2>
                </div>
            </div>

            <div className="container p-2">
                <div className="row">
                    <div className="col-sm-12">

                        <div className="form-group">
                            <label >Tên</label>
                            <input type="text" className="form-control" id="inp-fullname" placeholder="Nhập tên" onChange={tenChangeHandler}></input>
                        </div>
                        <div className="form-group">
                            <label >Email</label>
                            <input type="text" className="form-control" id="inp-email" placeholder="Nhập email" onChange={emailChangeHandler}></input>
                        </div>
                        <div className="form-group">
                            <label>Số điện thoại</label>
                            <input type="text" className="form-control" id="inp-dien-thoai" placeholder="Nhập số điện thoại" onChange={sodienthoaiChangeHandler}></input>
                        </div>
                        <div className="form-group">
                            <label >Địa chỉ</label>
                            <input type="text" className="form-control" id="inp-dia-chi" placeholder="Địa chỉ" onChange={diachiChangeHandler}></input>
                        </div>
                        <div className="form-group">
                            <label >Mã giảm giá (voucher ID)</label>
                            <input type="text" className="form-control" id="inp-voucher" placeholder="Voucher ID" onChange={voucherChangeHandler}></input>
                        </div>
                        <div className="form-group">
                            <label >Lời nhắn</label>
                            <input type="text" className="form-control" id="inp-message" placeholder="Lời nhắn" onChange={mesageChangeHandler}></input>
                        </div>


                    </div>
                </div>

            </div>

        </>
    )

}

export default FormComponent;
