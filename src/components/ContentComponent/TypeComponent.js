import { useEffect, useState } from "react";

import seafood from "../../images/seafood.jpg";
import hawaiian from "../../images/hawaiian.jpg";
import bacon from "../../images/bacon.jpg";

function TypeComponent(props) {
    const [TypePizza, setTypePizza] = useState("");
    const [colorBtn1, setcolorBtn1] = useState("btn btn-warning w-100");
    const [colorBtn2, setcolorBtn2] = useState("btn btn-warning w-100");
    const [colorBtn3, setcolorBtn3] = useState("btn btn-warning w-100");

    const btn1Click = (event) => {
        setTypePizza({
            loaiPizza: "OCEAN MANIA"
        });
        console.log({
            loaiPizza: "OCEAN MANIA"
        })
        setcolorBtn1("btn btn-success w-100")
        setcolorBtn2("btn btn-warning w-100")
        setcolorBtn3("btn btn-warning w-100")
    }
    const btn2Click = (event) => {
        setTypePizza({
            loaiPizza: "HAWAIIAN"
        });
        console.log({
            loaiPizza: "HAWAIIAN"
        })
        setcolorBtn1("btn btn-warning w-100")
        setcolorBtn2("btn btn-success w-100")
        setcolorBtn3("btn btn-warning w-100")
    }
    const btn3Click = (event) => {
        setTypePizza({
            loaiPizza: "CHEESY CHICKEN BACON"
        });
        console.log({
            loaiPizza: "CHEESY CHICKEN BACON"
        })
        setcolorBtn1("btn btn-warning w-100")
        setcolorBtn2("btn btn-warning w-100")
        setcolorBtn3("btn btn-success w-100")
    }
    useEffect(() => {
        props.parentCallback(TypePizza);

    }, [TypePizza])
    return (
        <>
            <div className="container" id="loaipizza">
                <div id="plans" className="row" id="combo">

                    <div className="col-sm-12 text-center p-4 mt-4">
                        <h2><span className="tieude365 p-2">Chọn loại pizza</span></h2>
                    </div>
                </div>
                <div className="container" style={{ padding: "10px 0 20px 0" }}>

                    <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-4">
                                <div className="card w-100" style={{ width: "18rem" }}>
                                    <img src={seafood} className="card-img-top"></img>
                                    <div className="card-body">
                                        <h4>OCEAN MANIA</h4>
                                        <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                        <p>Xốt Cà Chua, Phô Mai Mozzarella, Tôm, Mực, Thanh Cua, Hành Tây.
                                        </p>
                                        <p><button onClick={btn1Click} className= {colorBtn1}>Chọn</button></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card w-100" style={{ width: "18rem" }}>
                                    <img src={hawaiian} className="card-img-top"></img>
                                    <div className="card-body">
                                        <h4>HAWAIIAN</h4>
                                        <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                                        <p>Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.
                                        </p>
                                        <p><button onClick={btn2Click} className= {colorBtn2}>Chọn</button></p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="card w-100" style={{ width: "18rem" }}>
                                    <img src={bacon} className="card-img-top"></img>
                                    <div className="card-body">
                                        <h4>CHEESY CHICKEN BACON</h4>
                                        <p>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                        <p>Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.
                                        </p>
                                        <p><button onClick={btn3Click} className= {colorBtn3} >Chọn</button></p>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </>
    )

}

export default TypeComponent;
