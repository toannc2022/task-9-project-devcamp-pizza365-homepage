
import { useEffect, useState } from "react";

function DrinkComponent(props) {
    const [posts, setPosts] = useState([]);
    const [drink, setDrink] = useState("");

    const getData = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-pizza365/drinks");

        const data = await response.json();

        return data;
    }

    useEffect(() => {
        getData()
            .then((data) => {
                console.log(data);
                setPosts(data);
            })
            .catch((error) => {
                console.log(error);
            })
        props.parentCallback(drink);
    }, [drink])

    const drinkChangeHandler = (event, value) => {
        console.log(event.target.value)
        setDrink(event.target.value)

    }

    return (
        <>
            <div className="container">
                <div className="col-sm-12 text-center p-4 mt-4">
                    <h2><span className="tieude365 p-2">Chọn đồ uống</span></h2>
                </div>
                <select className="form-control" id="select-drink" onChange={drinkChangeHandler}>
                    <option value="ALL">Tất cả loại nước uống</option>
                    {posts.map((drink, index) => (
                        <option key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</option>
                    ))}

                </select>
            </div>

        </>
    )

}

export default DrinkComponent;
