
import { useEffect, useState } from "react";

function SizeComponent(props) {
    const [sizeCombo, setSizeCombo] = useState("");
    const [colorBtnS, setcolorBtnS] = useState("btn btn-warning w-100");
    const [colorBtnM, setcolorBtnM] = useState("btn btn-warning w-100");
    const [colorBtnL, setcolorBtnL] = useState("btn btn-warning w-100");


    const btnSClick = (event) => {
        setSizeCombo({
            kichCo: "S",
            duongKinh: "20cm",
            suon: "2",
            salad: "200g",
            soLuongNuoc: "2",
            thanhTien: "150000"
        });
        setcolorBtnS("btn btn-success w-100")
        setcolorBtnM("btn btn-warning w-100")
        setcolorBtnL("btn btn-warning w-100")
        console.log({
            kichCo: "S",
            duongKinh: "20cm",
            suon: "2",
            salad: "200g",
            soLuongNuoc: "2",
            thanhTien: "150000"
        })

    }
    const btnMClick = (event) => {
        setSizeCombo({
            kichCo: "M",
            duongKinh: "25cm",
            suon: "4",
            salad: "300g",
            soLuongNuoc: "3",
            thanhTien: "200000"
        });
        setcolorBtnS("btn btn-warning w-100")
        setcolorBtnM("btn btn-success w-100")
        setcolorBtnL("btn btn-warning w-100")
        console.log({
            kichCo: "M",
            duongKinh: "25cm",
            suon: "4",
            salad: "300g",
            soLuongNuoc: "3",
            thanhTien: "200000"
        })

    }
    const btnLClick = (event) => {
        setSizeCombo({
            kichCo: "L",
            duongKinh: "30cm",
            suon: "8",
            salad: "500g",
            soLuongNuoc: "4",
            thanhTien: "250000"
        });
        setcolorBtnS("btn btn-warning w-100")
        setcolorBtnM("btn btn-warning w-100")
        setcolorBtnL("btn btn-success w-100")
        console.log({
            kichCo: "L",
            duongKinh: "30cm",
            suon: "8",
            salad: "500g",
            soLuongNuoc: "4",
            thanhTien: "250000"
        })

    }
    useEffect(() => {
        props.parentCallback(sizeCombo);

    }, [sizeCombo])


    return (
        <>
            <div className="container">

                <div className="row" id="combo">

                    <div className="col-sm-12 text-center p-4 mt-4">
                        <h2>
                            <span className="menu p-2" style={{ borderBottom: "1px solid" }}>Chọn size pizza</span>
                        </h2>
                        <p><span className="menu">Chọn combo pizza phù hợp với nhu cầu của bạn.</span></p>
                    </div>
                    <div className="container">
                        <div className="col-sm-12">
                            <div className="row">
                                <div className="col-sm-4">
                                    <div className="card">
                                        <div className="card-header bg-warning text-white text-center">
                                            <h3>S</h3>
                                            <h6>(Small)</h6>
                                        </div>
                                        <div className="card-body text-center">
                                            <ul className="list-group list-group-flush">
                                                <li className="list-group-item">Đường kính: <b>20cm</b></li>
                                                <li className="list-group-item">Sườn nướng: <b>2</b></li>
                                                <li className="list-group-item">Salad: <b>200g</b></li>
                                                <li className="list-group-item">Nước ngọt: <b>2</b></li>
                                                <li className="list-group-item">VND: <h1><b>150.000</b></h1>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="card-footer text-center">
                                            <button onClick={btnSClick} className={colorBtnS}>Chọn</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="card">
                                        <div className="card-header bg-warning text-white text-center">
                                            <h3>M</h3>
                                            <h6>(Medium)</h6>
                                        </div>
                                        <div className="card-body text-center">
                                            <ul className="list-group list-group-flush">
                                                <li className="list-group-item">Đường kính: <b>25cm</b></li>
                                                <li className="list-group-item">Sườn nướng: <b>4</b></li>
                                                <li className="list-group-item">Salad: <b>300g</b></li>
                                                <li className="list-group-item">Nước ngọt: <b>3</b></li>
                                                <li className="list-group-item">VND: <h1><b>200.000</b></h1>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="card-footer text-center">
                                            <button onClick={btnMClick} className={colorBtnM}>Chọn</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-4">
                                    <div className="card">
                                        <div className="card-header bg-warning text-white text-center">
                                            <h3>L</h3>
                                            <h6>(Large)</h6>
                                        </div>
                                        <div className="card-body text-center">
                                            <ul className="list-group list-group-flush">
                                                <li className="list-group-item">Đường kính: <b>30cm</b></li>
                                                <li className="list-group-item">Sườn nướng: <b>8</b></li>
                                                <li className="list-group-item">Salad: <b>500g</b></li>
                                                <li className="list-group-item">Nước ngọt: <b>4</b></li>
                                                <li className="list-group-item">VND: <h1><b>250.000</b></h1>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="card-footer text-center">
                                            <button onClick={btnLClick} className={colorBtnL}>Chọn</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )

}

export default SizeComponent;
