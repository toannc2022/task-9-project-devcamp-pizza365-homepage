import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";



import HeaderComponent from "./components/Header.js";
import ContentComponent from "./components/Content.js";
import ContentFooter from "./components/Footer.js";


function App() {
  return (
    <div className="App">
      <HeaderComponent />
      <ContentComponent />
      <ContentFooter />
    </div>
  );
}

export default App;
